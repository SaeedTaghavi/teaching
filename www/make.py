"""Some ugly spaghetti to build the HTML pages"""

import glob
import mistune
import os
import shutil
import nbformat
import os
from nbconvert import HTMLExporter
import time
import re
from itertools import chain

DEST = '.'
FOLDERS = ['../python','../mathematics','../modeling','../adaptive_dynamics']

TEMPLATE = """        <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{title}</title>
<link rel='stylesheet' type='text/css' href='{static}/main.css'></head>
<body>
<header id="preamble">
<h1><a href='index.html'>Lecture Notes</a></h1> Collected lectures notes in computational biology
</header>
<main>
<nav>{breadcrumbs}</nav>
{content}
</main>"""+""" <footer> Collected lecture notes by <a href="https://www.normalesup.org/~doulcier/">Guilhem Doulcier</a> - <a href="https://gitlab.com/geeklhem/teaching"> Version control </a> - <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a> - Page generated on {} </footer> </html> """.format(time.asctime())

class TocMaker(mistune.Renderer):
    def __init__(self):
        self.toc = []
        mistune.Renderer.__init__(self)
    def header(self, text, level, raw=None):
        self.toc.append([text, level])
        return ''

def get_titles(nb):
    titles =[]
    text = '\n\n'.join([c['source'] for c in nb.cells if c['cell_type'] == 'markdown'])
    toc = TocMaker()
    mistune.Markdown(renderer=toc)(text)
    return toc.toc

def gen_toc(titles, minlevel=0, maxlevel=5, link=None):
    out = '<div class="toc">'
    for txt, level in titles:
        level += minlevel
        if level<maxlevel:
            if  level<=5:
                tag = 'h{}'.format(level)
                sep = ''
            else:
                tag = 'span'
                sep = '&emsp;'
            atag, atag_close = '', ''
            if link:
                anchor = re.sub(r'\<[a-z\/]*\>','',txt).replace(' ','-')
                atag = '<a href={}#{}>'.format(link,anchor)
                atag_close = '</a>'
            out += '<{0}>{2}{1}{3}{4}</{0}>\n'.format(tag,txt,atag,atag_close,sep)
    out += '</div>'
    return out


def parse_readme(readme_path):
    with open(readme_path, 'r') as file:
        readme_md = file.read()
    toc = TocMaker()
    readme_content = mistune.markdown(readme_md)
    mistune.Markdown(renderer=toc)(readme_md)
    title = toc.toc[0][0]
    return readme_content, title


def parse_notebook(nb_path, dest_nb, dest_html, breadcrumbs=''):
    nb = nbformat.read(nb_path, as_version=4)
    fname = os.path.basename(nb_path)

    # Save a copy of the notebook
    nbformat.write(nb, dest_nb)

    # Extract toc and title
    toc = get_titles(nb)
    if len(toc):
        title = toc[0][0]
        toc = toc[1:]

    # Export the HTML version
    html_exporter = HTMLExporter()
    html_exporter.template_file = 'cust.tpl'
    (body, _) = html_exporter.from_notebook_node(nb)

    # Filter cells
    filtered_cells = [cell for cell in nb['cells']
                      if not('tags' in cell['metadata'] and
                             ('solution' in cell['metadata']['tags']
                              or 'ta_notes' in cell['metadata']['tags']))]
    filtered = len(filtered_cells) != len(nb['cells'])

    # HTML export
    filtered_download = '<a href="{}_sujet.ipynb">notebook without solution</a>'.format(fname.split('.')[0]) if filtered else ''
    download = '> {0} [<a href="{1}.ipynb">Download notebook</a> {2}]'.format(title, fname.split('.')[0], filtered_download)
    breadcrumbs += download
    with open(dest_html, 'w') as file:
        file.write(TEMPLATE.format(content=body,
                                   title=title,
                                   breadcrumbs=breadcrumbs,
                                   static='../'))

    # Export filtered notebook
    if filtered:
        nb['cells'] = filtered_cells
        nbformat.write(nb, dest_nb.replace('.ipynb', '_sujet.ipynb'))

    return title, toc



def parse_folder(folder, dest=DEST):
    readme_path = os.path.join(folder, 'README.md')
    fname = os.path.basename(folder)
    dest_path = os.path.join(DEST, fname)
    short_title = fname.capitalize()
    breadcrumbs = '<a href="../index.html">Lecture notes</a> > <a href=".">{}</a>'.format(short_title)
    if os.path.exists(dest_path):
        shutil.rmtree(dest_path)
    os.mkdir(dest_path)

    for f in glob.glob(os.path.join(folder,"*.[sp][vn]g")):
        print(f)
        shutil.copyfile(f, os.path.join(dest_path, os.path.basename(f)))

    toc = ''
    for f in sorted(glob.glob(os.path.join(folder,"[0Edbe]*.ipynb"))):
        print(f)
        bname = os.path.basename(f)
        dest_f = os.path.join(dest_path, bname)
        html_f = dest_f.replace('.ipynb', '.html')
        relpath = os.path.relpath(html_f,dest_path)
        title_f, toc_f = parse_notebook(f, dest_f, html_f, breadcrumbs=breadcrumbs)
        toc += '<h2><a href={}>{}</a></h2>'.format(relpath, title_f)
        toc += gen_toc(toc_f, minlevel=3, maxlevel=10, link=relpath)

    if os.path.exists(readme_path):
        readme_content, title = parse_readme(readme_path)
    else:
        readme_content, title = '', short_title

    index_content = readme_content + toc
    index_path = os.path.join(dest_path, 'index.html')
    with open(index_path,'w') as html:
         html.write(TEMPLATE.format(content=index_content,
                                    title=title,
                                    static='../',
                                    breadcrumbs=breadcrumbs))
    return short_title, title, fname

ROOT = '../'
TOC ='<div class="toc">'
for f in FOLDERS:
    print('Folder:', f)
    short_title, title, fname = parse_folder(f)
    TOC += '<h2><a href="{}">{}</h2>\n'.format(fname, title)
TOC += '</div>'
readme_path = os.path.join(ROOT, 'README.md')
content,title = parse_readme(readme_path)
content += TOC
with open(os.path.join(DEST, 'index.html'),'w') as html:
    html.write(TEMPLATE.format(content=content,
                               title=title,
                               static='./',
                               breadcrumbs=''))

# Mathématiques

The lectures "Mathematics: What a biologist should not ignore" are
dispensed by Pr. Amaury Lambert in the Biology department of the École
normale supérieure de Paris. I taught the tutorial session "what a
biologist should not forget" along with Dr. Pierre Vincens in 2016,
2017 and 2018.

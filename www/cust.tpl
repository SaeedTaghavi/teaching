{% extends 'basic.tpl'%}
{% from 'mathjax.tpl' import mathjax %}


{%- block header -%}
{%- block html_head -%}
<script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.10/require.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

{% block ipywidgets %}
{%- if "widgets" in nb.metadata -%}
<script>
(function() {
  function addWidgetsRenderer() {
    var mimeElement = document.querySelector('script[type="application/vnd.jupyter.widget-view+json"]');
    var scriptElement = document.createElement('script');
    var widgetRendererSrc = '{{ resources.ipywidgets_base_url }}@jupyter-widgets/html-manager@*/dist/embed-amd.js';
    var widgetState;
    // Fallback for older version:
    try {
      widgetState = mimeElement && JSON.parse(mimeElement.innerHTML);
      if (widgetState && (widgetState.version_major < 2 || !widgetState.version_major)) {
	widgetRendererSrc = '{{ resources.ipywidgets_base_url }}jupyter-js-widgets@*/dist/embed.js';
      }
    } catch(e) {}
    scriptElement.src = widgetRendererSrc;
    document.body.appendChild(scriptElement);
  }
  document.addEventListener('DOMContentLoaded', addWidgetsRenderer);
}());
</script>
{%- endif -%}
{% endblock ipywidgets %}

{% for css in resources.inlining.css -%}
    <style type="text/css">
    {{ css }}
    </style>
{% endfor %}


<!-- Loading mathjax macro -->
{{ mathjax() }}
{%- endblock html_head -%}
{%- endblock header -%}

{% block body %}
 {{ super() }}
{%- endblock body %}

{% block any_cell %}
{% if 'solution' in cell['metadata'].get('tags', []) %}
    <div class="solution_cell">
	{{ super() }}
    </div>
{% elif 'ta_notes' in cell['metadata'].get('tags', []) %}
    <div class="ta_notes">
	{{ super() }}
    </div>

{% elif 'aside' in cell['metadata'].get('tags', []) %}
    <div class="aside_cell">
	{{ super() }}
    </div>
{% elif 'clue' in cell['metadata'].get('tags', []) %}
    <div style="clue_cell">
	{{ super() }}
    </div>
{% else %}
    {{ super() }}
{% endif %}
{% endblock any_cell %}
